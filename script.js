/*
  Реалізувати функцію підсвічування клавіш. 
  Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:

- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, 
  повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше 
  була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter 
  перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, 
  і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
*/

"use strict";

const buttonsWrapper = document.querySelector(".btn-wrapper");
const buttons = buttonsWrapper.querySelectorAll(".btn");
const buttonsCodes = {};
buttons.forEach((button) => {
  buttonsCodes[button.dataset.keyCode] = button;
});

function buttonsControl({ code: keyCode }) {
  //console.log(keyCode);
  for (let code in buttonsCodes) {
    code === keyCode
      ? buttonsCodes[code].classList.add("active")
      : buttonsCodes[code].classList.remove("active");
  }
}
document.addEventListener("keypress", buttonsControl);
